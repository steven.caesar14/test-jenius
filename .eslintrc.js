module.exports = {
  root: true,
  extends: 'airbnb',
  plugins: [
      'react',
      'react-native',
      'react-hooks'
  ],
  parser: 'babel-eslint',
  env: {
      jest: true,
      es6: true,
      'react-native/react-native': true
  },
  rules: {
      'arrow-body-style': ['error', 'as-needed'],
      camelcase: 'off',
      'consistent-return': 'off',
      'no-param-reassign': 0,
      'comma-dangle': ['error', 'never'],
      indent: ['error', 4, { ignoredNodes: ['JSXElement'], SwitchCase: 1 }],
      'import/order': 'off',
      'import/no-mutable-exports': 'off',
      'import/no-cycle': 'off',
      'import/extensions': 'off',
      'no-console': 'off',
      'no-nested-ternary': ['error'],
      'no-plusplus': 'off',
      'no-shadow': 'off',
      'no-use-before-define': ['error', { variables: false }],
      'no-underscore-dangle': 'off',
      'no-useless-escape': 'off',
      'no-cond-assign': 'off',
      'max-len': 'off',
      'prefer-destructuring': ['error', {
          AssignmentExpression: {
              array: false,
              object: false
          }
      }],
      'prefer-promise-reject-errors': 'off',
      'react/no-this-in-sfc': 0,
      'react/forbid-prop-types': 0,
      'react/destructuring-assignment': 0,
      'react/no-access-state-in-setstate': 'off',
      'react/jsx-props-no-spreading': 'off',
      'react/jsx-indent': ['error', 4],
      'react/jsx-indent-props': ['error', 4],
      'react/jsx-filename-extension': 'off',
      'react/jsx-one-expression-per-line': 'off'
  },
  globals: {
      fetch: false
  },
  overrides: [
      {
          files: ['*.ts', '*.tsx'],
          parser: '@typescript-eslint/parser',
          parserOptions: {
              project: './tsconfig.json'
          },
          plugins: [
              'react',
              '@typescript-eslint'
          ],
          rules: {
              'no-use-before-define': 'off',
              'react/require-default-props': 'off'
          }
      }
  ],
  settings: {
      'import/resolver': {
          node: {
              extensions: ['.js', '.jsx', '.ts', '.tsx']
          }
      }
  }
};
