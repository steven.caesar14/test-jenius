import * as React from 'react';
import { Platform } from 'react-native';
import HomeScreen from './src/app/HomeScreen';
import ContactForm from './src/app/ContactForm';
import { HeaderBackground, HeaderNavigation } from './src/navigation/Header';
import { colors } from './src/resources';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

const Stack = createStackNavigator();

const App = () => (
    <NavigationContainer>
        <Stack.Navigator
            initialRouteName="Home"
            screenOptions={({ navigation }) => ({
                gestureEnabled: false,
                headerLeft: () => HeaderNavigation(navigation),
                cardStyle: { backgroundColor: colors.WHITE },
                headerBackground: () => HeaderBackground(),
                headerTitleStyle: {
                    fontSize: 18,
                    fontFamily: 'NunitoSans-ExtraBold',
                    fontWeight: Platform.OS === 'ios' ? '800' : '200'
                }
            })}
        >
            <Stack.Screen name="HomeScreen" options={{ headerShown: false }} component={HomeScreen} />
            <Stack.Screen name="ContactForm" component={ContactForm} />
        </Stack.Navigator>
    </NavigationContainer>
);

export default App;
