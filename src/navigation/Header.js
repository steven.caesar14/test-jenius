import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import { images, colors } from '../resources';

export const HeaderBackground = () => (
    <View
        style={{
            flex: 1,
            backgroundColor: colors.WHITE,
            borderBottomWidth: 1,
            borderBottomColor: '#f1f2f2'
        }}
    />
);

export const HeaderNavigation = (param) => (
    <TouchableOpacity
        style={{
            width: 40,
            height: 40,
            marginLeft: 10,
            justifyContent: 'center',
            alignItems: 'center'
        }}
        onPress={() => {
            if (!param || Object.keys(param).keys === 0) {
                return new Error('Parameter tidak boleh kosong');
            }

            if (!!param.onPress && param.onPress instanceof Function) {
                param.onPress();
                return;
            }

            param.pop();
        }}
    >
        <Image
            style={{
                width: 24,
                height: 24,
                resizeMode: 'contain'
            }}
            resizeMode="contain"
            source={param?.icon ?? images.leftArrow}
        />
    </TouchableOpacity>
);
