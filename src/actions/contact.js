import API from '../services/APIs';
import Promise from 'promise';

export const getAllContacts = () => new Promise((resolve, reject) => {
    API().getAllContacts().then((response) => {
        const responseJSON = response.data;
        if (!response.ok) {
            reject('Connection Failed');
        }
        if (response.status !== 200) {
            reject('Connection Error');
        }
        resolve(responseJSON);
    });
});

export const removeContact = (id) => new Promise((resolve, reject) => {
    console.log('ID DELETE', id);
    API().deleteContact(id).then((response) => {
        const responseJSON = response.data;
        console.log('RESPONSE DELETE', response);
        if (!response.ok && response.status !== 202) {
            const message = responseJSON?.message || 'Connection Failed';
            reject(message);
        }
        resolve(responseJSON);
    }).catch((errorResponse) => {
        console.log('ERROR', errorResponse);
        reject('Connection Error');
    });
});

export const editContact = (params) => new Promise((resolve, reject) => {
    API().editRequest(params).then((response) => {
        console.log('res EDIT', response);
        const responseJSON = response.data;
        if (!response.ok && response.status !== 200) {
            const message = responseJSON.message ? responseJSON.message : `Failed to edit contact ${response.status}`;
            reject(message);
        }
        resolve(responseJSON);
    }).catch((errorResponse) => {
        console.log('ERROR', errorResponse);
        reject('Connection Error');
    });
});

export const saveContact = (params) => new Promise((resolve, reject) => {
    API().addContact(params).then((response) => {
        console.log('res ADD', response);
        console.log('res STATUS', response.status);
        const responseJSON = response.data;
        if (!response.ok && responseJSON.status !== 200) {
            const message = responseJSON.message ? responseJSON.message : `Failed to add contact ${response.status}`;
            reject(message);
        }
        resolve(responseJSON);
    }).catch((errorResponse) => {
        console.log('ERROR', errorResponse);
        reject('Connection Error');
    });
});
