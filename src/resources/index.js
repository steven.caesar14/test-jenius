import images from './Images';
import colors from './Colors';
import Styles from './Styles';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

export {
    images, colors, Icon, Styles
};
