import colors from './Colors';

const Styles = {
    globalModalButton: {
        greenButton: {
            button: {
                paddingVertical: 7,
                backgroundColor: '#004e44',
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 10,
                marginRight: 10,
                marginBottom: 16,
                borderRadius: 4
            },
            text: {
                fontWeight: 'bold',
                color: colors.WHITE,
                fontSize: 14,
                lineHeight: 21
            }
        },
        whiteButton: {
            button: {
                paddingVertical: 7,
                backgroundColor: colors.WHITE,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: 10,
                marginRight: 10,
                marginBottom: 9,
                borderRadius: 4,
                borderColor: colors.primaryColorDark,
                borderWidth: 1
            },
            text: {
                fontWeight: 'bold',
                color: colors.primaryColorDark,
                fontSize: 14,
                lineHeight: 21
            }
        }
    }
};
export default Styles;
