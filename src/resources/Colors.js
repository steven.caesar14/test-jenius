const colors = {
    WHITE: '#FFFFFF',
    BLACK: '#000000',
    GOLD: '#FBC02D',
    RED: '#F44336',
    primaryColor: '#00ab4e',
    primaryColorDark: '#004e44',
    GREENLIME: '#009688',
    DARK: '#212121',
    GREY: '#757575'
};
export default colors;
