import { create } from 'apisauce';

const BASE_URL = 'https://simple-contact-crud.herokuapp.com';

const API = () => {
    const API = create({
        baseURL: BASE_URL,
        headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
        }
    });

    const getAllContacts = () => API.get('/contact');

    const getContactsByID = (id) => API.get('/contact', { params: { id } });

    const deleteContact = (id) => API.delete(`/contact/${id}`);

    const addContact = (params) => API.post('/contact',
        {
            age: params.age,
            firstName: params.firstName,
            lastName: params.lastName,
            photo: params.photo
        });

    const editRequest = (params) => API.put(`/contact/${params.id}`,
        {
            age: params.age,
            firstName: params.firstName,
            lastName: params.lastName,
            photo: params.photo
        });

    return {
        getAllContacts,
        getContactsByID,
        addContact,
        editRequest,
        deleteContact
    };
};

export default API;
