import React, { Component } from 'react';
import {
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import PropTypes from 'prop-types';
import { colors, Icon } from '../resources';

export default class GlobalModal extends Component {
    _renderButton = () => (
        this.props.buttons.map((v) => (
            <TouchableOpacity style={v.style.button} onPress={() => v.callback()} key={v.title}>
                <Text style={v.style.text}>{v.title}</Text>
            </TouchableOpacity>
        ))
    )

    _renderContent = () => (
        <>
            <TouchableOpacity style={styles.closeButton} onPress={() => { this.props.onClose(); }}>
                <Icon name="times" size={20} color={colors.GREY} />
            </TouchableOpacity>
            <View style={styles.modalContent}>
                <Text style={styles.modalTitle}>{this.props.modalTitle}</Text>
                <Text style={styles.modalDescription}>{this.props.modalContent}</Text>
            </View>
            {this._renderButton()}
        </>
    )

    render = () => (
        <Modal
            animationType="fade"
            transparent
            visible={this.props.isVisible}
        >
            <View style={styles.modalMainContainer}>
                <View style={styles.modalContentContainer}>
                    {this._renderContent()}
                </View>
            </View>
        </Modal>
    );
}

GlobalModal.propTypes = {
    modalTitle: PropTypes.string,
    modalContent: PropTypes.string,
    isVisible: PropTypes.bool,
    buttons: PropTypes.array,
    onClose: PropTypes.any.isRequired
};

GlobalModal.defaultProps = {
    modalTitle: '',
    modalContent: '',
    isVisible: false,
    buttons: []
};

const styles = StyleSheet.create({

    modalMainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.3)',
        paddingHorizontal: 32
    },

    modalContentContainer: {
        backgroundColor: colors.WHITE,
        borderRadius: 12
    },

    modalContent: {
        marginBottom: 1,
        paddingVertical: 16,
        paddingHorizontal: 12
    },

    closeButton: {
        position: 'absolute',
        right: 16,
        top: 10,
        zIndex: 1
    },

    modalTitle: {
        textAlign: 'center',
        color: colors.DARK,
        fontSize: 18,
        marginTop: 16,
        marginBottom: 8,
        fontWeight: 'bold',
        lineHeight: 22
    },

    modalDescription: {
        textAlign: 'center',
        color: colors.GREY,
        fontSize: 14,
        marginBottom: 8,
        lineHeight: 20

    }
});
