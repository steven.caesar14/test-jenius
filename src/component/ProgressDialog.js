import React from 'react';
import {
    Modal,
    Platform,
    StyleSheet,
    Text,
    TouchableOpacity,
    View
} from 'react-native';
import { colors, Icon } from '../resources';
import Spinner from 'react-native-spinkit';
import PropType from 'prop-types';

class ProgressDialog extends React.Component {
    _renderLoading = () => {
        if (this.props.type === 'loading') {
            return (
                <Spinner
                    style={{ marginBottom: 4 }}
                    isVisible
                    size={42}
                    type="ThreeBounce"
                    color="#00ab4e"
                />
            );
        } if (this.props.type === 'error') {
            return <Icon name="times-circle" size={42} color={colors.RED} />;
        } if (this.props.type === 'warning') {
            return <Icon name="exclamation-triangle" size={42} color={colors.GOLD} />;
        } if (this.props.type === 'success') {
            return <Icon name="check-circle" size={42} color={colors.primaryColorDark} />;
        }
    };

    render() {
        return (
            <Modal
                animationType="fade"
                transparent
                visible={this.props.visible}
                onRequestClose={() => true}
            >
                <View style={styles.outerContainer}>
                    <View style={[styles.innerContainer]}>
                        <View style={{ flex: -1 }}>
                            <View style={{
                                alignItems: 'center', paddingTop: 16, paddingHorizontal: 16, paddingBottom: 8
                            }}
                            >
                                {this._renderLoading()}
                                <Text style={{
                                    color: colors.BLACK,
                                    textAlign: 'center',
                                    marginTop: 4,
                                    lineHeight: Platform.OS === 'ios' ? 16 : 22
                                }}
                                >{this.props.message}
                                </Text>
                            </View>
                            {(this.props.type !== 'loading') && (
                                <View style={styles.buttonWrapper}>
                                    <TouchableOpacity
                                        onPress={() => {
                                            if (this.props.onSuccess) {
                                                return this.props.onSuccess();
                                            }
                                            this.props.onClose();
                                        }}
                                        style={styles.buttonContainer}
                                    >
                                        <Text style={styles.buttonOK}>OK</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                        </View>
                    </View>
                </View>
            </Modal>
        );
    }
}

ProgressDialog.propTypes = {
    visible: PropType.bool.isRequired,
    message: PropType.string,
    type: PropType.any,
    onClose: PropType.any.isRequired,
    onSuccess: PropType.any
};

ProgressDialog.defaultProps = {
    onSuccess: null,
    message: '',
    type: null
};

export default ProgressDialog;

export const ProgressDialogType = {
    SUKSES: 'success',
    ERROR: 'error',
    WARNING: 'warning',
    LOADING: 'loading',

    PESAN_PROSES_GENERIK: 'Please Wait',
    PESAN_SUKSES_GENERIK: 'Success',
    PESAN_GAGAL_GENERIK: 'Please try again!',

    PESAN_OKE_GENERIK: 'Oke',
    PESAN_TERJADI_KESALAHAN_GENERIK: 'Terjadi Kesalahan'
};

const styles = StyleSheet.create({
    outerContainer: {
        backgroundColor: 'rgba(0, 0, 0, .6)',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 32
    },
    innerContainer: {
        backgroundColor: colors.WHITE,
        borderRadius: 2,
        flexDirection: 'row'
    },
    buttonWrapper: {
        alignItems: 'flex-end',
        paddingHorizontal: 16,
        paddingBottom: 8
    },
    buttonContainer: {
        paddingHorizontal: 4,
        paddingVertical: 4
    },
    buttonOK: {
        fontSize: 14,
        marginVertical: 4,
        color: colors.primaryColorDark
    }
});
