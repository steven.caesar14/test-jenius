import React, { Component } from 'react';
import {
    KeyboardAvoidingView,
    StyleSheet,
    Text,
    TextInput,
    TouchableOpacity,
    View,
    Image,
    Platform,
    ScrollView
} from 'react-native';
import PropType from 'prop-types';
import { colors, Icon, Styles } from '../resources';
import ImagePicker from 'react-native-image-crop-picker';
import { editContact, saveContact } from '../actions/contact';
import ProgressDialog, { ProgressDialogType } from '../component/ProgressDialog';

export default class ContactForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: null,
            input: {
                firstName: null,
                lastName: null,
                age: null,
                photo: null
            },
            flagState: 'ADD',
            errorImage: false,
            dialogMessage: null,
            dialogType: null,
            showDialog: false,
            buttonDisabled: false
        };
    }

    componentDidMount = () => {
        if (__DEV__) console.log('Props Navigation', this.props.route.params);
        this.setState({
            ...this.state,
            flagState: this.props.route.params?.flagState,
            id: this.props.route.params?.item?.id || null,
            input: {
                ...this.state.input,
                firstName: this.props.route.params?.item?.firstName || null,
                lastName: this.props.route.params?.item?.lastName || null,
                age: this.props.route.params?.item?.age || null,
                photo: this.props.route.params?.item?.photo || null
            }
        });
    }

    submit = () => {
        this.showDialog(ProgressDialogType.LOADING, ProgressDialogType.PESAN_PROSES_GENERIK);
        const params = {
            id: this.state.id || null,
            firstName: this.state.input.firstName,
            lastName: this.state.input.lastName,
            age: Number(this.state.input.age),
            photo: this.state.input.photo
        };
        if (this.state.flagState === 'EDIT') {
            return editContact(params).then((response) => {
                this.showDialog(ProgressDialogType.SUKSES, response.message);
            }).catch((error) => {
                this.showDialog(ProgressDialogType.ERROR, error);
            });
        }

        saveContact(params).then((response) => {
            this.showDialog(ProgressDialogType.SUKSES, response.message);
        }).catch((error) => {
            this.showDialog(ProgressDialogType.ERROR, error);
        });
    }

    backScreen = () => {
        this.hideDialog();
        this.props.navigation.push('HomeScreen');
    }

    inputHandler = (key, value) => {
        this.setState({
            ...this.state,
            input: {
                ...this.state.input,
                [key]: value
            }
        });
    }

    imagePicker = () => {
        ImagePicker.openCamera({
            width: 400,
            height: 400,
            cropping: false,
            includeBase64: true,
            compressImageQuality: 0.5
        }).then((image) => {
            this.setState({
                ...this.state,
                input: {
                    ...this.state.input,
                    photo: image.path
                }
            }, () => {
                if (this.state.errorImage) {
                    this.setState({ errorImage: false });
                }
            });
        });
    }

    showDialog = (type, message) => {
        this.setState({
            dialogType: type,
            dialogMessage: message,
            showDialog: true
        });
    }

    hideDialog = () => {
        this.setState({ showDialog: !this.state.showDialog });
    }

    getDisabledButton = () => {
        const disabled = Object.keys(this.state.input).find((key) => !this.state.input[key]);
        return disabled;
    }

    renderImage = () => {
        if (this.state.errorImage) {
            return (
                <View style={{ marginTop: 12 }}>
                    <TouchableOpacity
                        style={{ justifyContent: 'center', marginRight: 12 }}
                        onPress={() => this.imagePicker()}
                    >
                        <View style={{ flexDirection: 'row' }}>
                            <Icon name="photo" size={32} color={colors.primaryColorDark} />
                            <View style={{
                                flexDirection: 'row',
                                paddingVertical: 4
                            }}
                            >
                                <Text style={{ marginLeft: 10 }}>Add Photo</Text>
                            </View>
                        </View>
                    </TouchableOpacity>
                </View>
            );
        }
        return (
            <View style={{ marginTop: 12 }}>
                <TouchableOpacity
                    style={{ justifyContent: 'center', marginRight: 12 }}
                    onPress={() => this.imagePicker()}
                >
                    <View>
                        <Image
                            style={styles.displayedImage}
                            resizeMethod="scale"
                            source={{ uri: `${this.state.input.photo}` }}
                            onError={(error) => {
                                console.log('Error image', error);
                                this.setState({ errorImage: true });
                            }}
                        />
                        <View style={{
                            justifyContent: 'center',
                            paddingVertical: 4
                        }}
                        >
                            <Text style={{ marginTop: 10, textAlign: 'center' }}>Click to change Photo</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS === 'ios' ? 'position' : null}
                style={{ flex: 1 }}
            >
                <ProgressDialog
                    type={this.state.dialogType}
                    message={this.state.dialogMessage}
                    visible={this.state.showDialog}
                    onClose={this.hideDialog}
                    onSuccess={this.backScreen}
                />
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <View style={{ flex: 1 }}>
                        <View style={styles.modalContent}>
                            <View>
                                <Text style={styles.formTitle}>First Name</Text>
                                <TextInput
                                    style={styles.inputText}
                                    onChangeText={(value) => {
                                        this.inputHandler('firstName', value);
                                    }}
                                    value={this.state.input.firstName}
                                    placeholder="First Name"
                                />
                            </View>
                            <View>
                                <Text style={styles.formTitle}>Last Name</Text>
                                <TextInput
                                    style={styles.inputText}
                                    onChangeText={(value) => {
                                        this.inputHandler('lastName', value);
                                    }}
                                    value={this.state.input.lastName}
                                    placeholder="Last Name"
                                />
                            </View>
                            <View>
                                <Text style={styles.formTitle}>Age</Text>
                                <TextInput
                                    style={styles.inputText}
                                    onChangeText={(value) => {
                                        this.inputHandler('age', value);
                                    }}
                                    value={this.state.input.age && String(this.state.input.age)}
                                    keyboardType="number-pad"
                                    placeholder="Age"
                                />
                            </View>
                            <View>
                                <Text style={styles.formTitle}>PHOTO</Text>
                                {this.renderImage()}

                            </View>
                        </View>
                        <View style={{ flex: 1, marginTop: 24 }}>
                            <TouchableOpacity
                                disabled={this.getDisabledButton()}
                                style={[Styles.globalModalButton.whiteButton.button, (this.getDisabledButton() && { backgroundColor: colors.GREY })]}
                                onPress={() => this.submit()}
                            >
                                <Text style={Styles.globalModalButton.whiteButton.text}>Submit</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={Styles.globalModalButton.greenButton.button}
                                onPress={() => this.props.navigation.pop()}
                            >
                                <Text style={Styles.globalModalButton.greenButton.text}>Cancel</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </KeyboardAvoidingView>
        );
    }
}

ContactForm.propTypes = {
    navigation: PropType.any.isRequired,
    route: PropType.any.isRequired
};

const styles = StyleSheet.create({

    displayedImage: {
        width: 120,
        height: 120,
        resizeMode: 'cover',
        alignSelf: 'center'
    },

    modalContent: {
        marginBottom: 1,
        paddingVertical: 16,
        paddingHorizontal: 12
    },

    closeButton: {
        position: 'absolute',
        right: 16,
        top: 10,
        zIndex: 1
    },

    formTitle: {
        textAlign: 'left',
        color: colors.DARK,
        fontSize: 14,
        marginTop: 16,
        marginBottom: 8,
        fontWeight: 'bold',
        lineHeight: 22
    },

    inputText: {
        borderColor: colors.BLACK,
        borderWidth: 1,
        textAlign: 'left',
        color: colors.GREY,
        fontSize: 14,
        marginBottom: 8,
        lineHeight: 20
    },

    modalDescription: {
        color: colors.BLACK,
        fontSize: 14,
        marginBottom: 8,
        lineHeight: 20

    }
});
