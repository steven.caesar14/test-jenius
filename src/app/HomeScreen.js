import React, { Component } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    TouchableOpacity,
    View,
    Text,
    ScrollView,
    RefreshControl,
    BackHandler
} from 'react-native';
import PropType from 'prop-types';
import { colors, Icon, Styles } from '../resources';
import { getAllContacts, removeContact } from '../actions/contact';
import ProgressDialog, { ProgressDialogType } from '../component/ProgressDialog';
import GlobalModal from '../component/GlobalModal';

class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Gadai Tabungan Emas',
        headerTintColor: 'white',
        headerStyle: {
            backgroundColor: colors.primaryColor
        }
    };

    constructor(props) {
        super(props);
        this.state = {
            contact: {
                index: -1,
                data: []
            },
            dialogMessage: null,
            dialogType: null,
            showDialog: false,

            isPullToRefresh: false,
            modal: {
                title: '',
                description: '',
                button: [],
                visible: false
            },
            showContactForm: false
        };
    }

    componentDidMount = () => {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        this.getAll();
        console.log('props navigation', this.props);
    }

    componentWillUnmount = () => {
        BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton = () => {
        BackHandler.exitApp();
    }

    onRefresh = () => {
        this.setState({
            ...this.state,
            isPullToRefresh: true,
            contact: {
                ...this.state.contact,
                data: []
            }
        }, () => {
            this.setState({
                ...this.state,
                isPullToRefresh: false
            }, () => {
                this.getAll();
            });
        });
    }

    getAll = () => {
        if (this.state.modal.visible) {
            this.hideModal();
        }
        this.showDialog(ProgressDialogType.LOADING, ProgressDialogType.PESAN_PROSES_GENERIK);
        getAllContacts().then((response) => {
            this.hideDialog();
            if (__DEV__) console.log('GET ALL', response);
            this.setState({
                ...this.state,
                contact: {
                    ...this.state.contact,
                    data: response.data
                }
            });
        }).catch((error) => {
            if (__DEV__) console.log('ERROR GET ALL', error);
            this.showDialog(ProgressDialogType.ERROR, error);
        });
    }

    confirmDelete = (id, index) => {
        const modalButton = [
            {
                title: 'Yes',
                callback: () => this.deleteContact(id, index),
                style: Styles.globalModalButton.whiteButton
            },
            {
                title: 'Cancel',
                callback: () => {
                    this.hideModal();
                },
                style: Styles.globalModalButton.greenButton
            }
        ];
        this.initModalContent(
            'Delete Contact',
            'Are you sure want to delete this contact?',
            modalButton,
            true
        );
    }

    deleteContact = (id, index) => {
        if (this.state.modal.visible) {
            this.hideModal();
        }
        if (__DEV__) console.log('PARAMS DELETE', id);
        this.showDialog(ProgressDialogType.LOADING, ProgressDialogType.PESAN_PROSES_GENERIK);
        removeContact(id).then((response) => {
            this.hideDialog();
            if (__DEV__) console.log('REMOVE CONTACT', response);
            this.setState({ selectedIndex: index }, () => this.removeIndex());
        }).catch((error) => {
            if (__DEV__) console.log('ERROR REMOVE CONTACT', error);
            this.showDialog(ProgressDialogType.ERROR, error);
        });
    }

    removeIndex = () => {
        const temp = this.state.contact.data.filter((i) => (i !== this.state.selectedIndex));
        this.setState({
            ...this.state,
            contact: {
                ...this.state.contact,
                data: temp
            }
        });
    }

    showDialog = (type, message) => {
        this.setState({
            dialogType: type,
            dialogMessage: message,
            showDialog: true
        });
    }

    hideDialog = () => {
        this.setState({ showDialog: !this.state.showDialog });
    }

    initModalContent = (title, description, button, visible) => {
        this.setState({
            ...this.state,
            modal: {
                ...this.state.modal,
                title,
                description,
                button,
                visible
            }
        });
    }

    hideModal = () => {
        this.setState({
            ...this.state,
            modal: {
                ...this.state.modal,
                visible: false
            }
        });
    }

    hideForm = () => {
        this.setState({
            ...this.state,
            showContactForm: false
        });
    }

    navigateToForm = (params) => {
        if (params.flag === 'EDIT') {
            return this.props.navigation.navigate('ContactForm', { item: params.data, flagState: params.flag });
        }
        this.props.navigation.navigate('ContactForm', { flagState: params.flag });
    }

    renderModal = () => (
        <GlobalModal
            modalTitle={this.state.modal.title}
            modalContent={this.state.modal.description}
            isVisible={this.state.modal.visible}
            buttons={this.state.modal.button}
            onClose={this.hideModal}
        />
    )

    renderContact = () => this.state.contact.data.map((item, index) => (
        <View style={styles.contact} key={item.id}>
            <View style={{ alignContent: 'center', flexDirection: 'row' }}>
                <View style={{ justifyContent: 'center', marginRight: 12 }}>
                    <Icon name="users" size={32} color={colors.primaryColorDark} />
                </View>
                <View>
                    <View style={styles.itemSection}>
                        <Text style={styles.title}>First Name</Text>
                        <Text style={styles.content}>{item?.firstName ?? ''}</Text>
                    </View>
                    <View style={styles.itemSection}>
                        <Text style={styles.title}>Last Name</Text>
                        <Text style={styles.content}>{item?.lastName ?? ''}</Text>
                    </View>
                    <View style={styles.itemSection}>
                        <Text style={styles.title}>Age</Text>
                        <Text style={styles.content}>{item?.age ?? ''}</Text>
                    </View>
                </View>
            </View>
            <View style={{ justifyContent: 'space-between', flexDirection: 'row', marginTop: 10 }}>
                <TouchableOpacity
                    onPress={() => {
                        const params = {
                            data: item,
                            flag: 'EDIT'
                        };
                        this.navigateToForm(params);
                    }}
                    style={[styles.buttonContainer, { backgroundColor: colors.GREENLIME, marginStart: 24 }]}
                >
                    <Text style={styles.buttonText}>Edit</Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={() => this.confirmDelete(item.id, index)} style={[styles.buttonContainer, { backgroundColor: colors.RED, marginEnd: 24 }]}>
                    <Text style={styles.buttonText}>Delete</Text>
                </TouchableOpacity>
            </View>
        </View>
    ));

    render = () => (
        <SafeAreaView style={{ flex: 1 }}>
            {this.state.modal.visible && this.renderModal()}
            <ProgressDialog
                type={this.state.dialogType}
                message={this.state.dialogMessage}
                visible={this.state.showDialog}
                onClose={this.hideDialog}
            />
            <View style={{ padding: 24 }}>
                <TouchableOpacity
                    style={{ justifyContent: 'center', marginRight: 12 }}
                    onPress={() => {
                        const params = {
                            data: null,
                            flag: 'ADD'
                        };
                        this.navigateToForm(params);
                    }}
                >
                    <View style={{ flexDirection: 'row' }}>
                        <Icon name="user-plus" size={32} color={colors.primaryColorDark} />
                        <View style={styles.itemSection}>
                            <Text style={{ marginLeft: 10 }}>Add Contact</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
            <ScrollView
                refreshControl={(
                    <RefreshControl
                        refreshing={this.state.isPullToRefresh}
                        onRefresh={() => {
                            this.onRefresh();
                        }}
                    />
                )}
            >
                {this.state.contact.data.length > 0 && this.renderContact()}
            </ScrollView>

        </SafeAreaView>
    );
}

HomeScreen.propTypes = {
    navigation: PropType.any.isRequired
};

const styles = StyleSheet.create({
    contact: {
        padding: 10,
        borderWidth: 2,
        borderColor: colors.primaryColor,
        marginVertical: 10,
        marginHorizontal: 10,
        borderRadius: 12
    },
    itemSection: {
        flexDirection: 'row',
        paddingVertical: 4
    },
    title: {
        fontSize: 12,
        fontWeight: 'bold',
        justifyContent: 'flex-start',
        width: '40%'
    },
    content: {
        fontSize: 12,
        fontWeight: 'bold',
        justifyContent: 'flex-start'
    },
    buttonContainer: {
        elevation: 8,
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 40
    },
    buttonText: {
        fontSize: 12,
        color: '#fff',
        fontWeight: 'bold',
        alignSelf: 'center',
        textTransform: 'uppercase'
    }
});

export default HomeScreen;
